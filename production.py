# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

__all__ = ['Production']


class Production:
    __name__ = 'production'
    __metaclass__ = PoolMeta

    def explode_bom(self):
        super(Production, self).explode_bom()
        if self.warehouse and self.warehouse.production_input_location:
            input_location = self.warehouse.production_input_location
            for move in self.inputs:
                if move.from_location != input_location.id:
                    move.from_location = input_location.id

    def set_moves(self):
        super(Production, self).set_moves()
        Move = Pool().get('stock.move')
        if self.warehouse.production_input_location:
            storage_location = self.warehouse.storage_location
            to_write = []
            for move in self.inputs:
                if move.from_location == storage_location:
                    to_write.append(move)
            if to_write:
                input_location = self.warehouse.production_input_location
                Move.write(to_write, {
                        'to_location': input_location.id,
                         })
