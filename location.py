# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Location']


class Location:
    __name__ = 'stock.location'
    __metaclass__ = PoolMeta

    production_input_location = fields.Many2One('stock.location', 'Production Intput',
                                                states={
                                                    'invisible': Eval('type') != 'warehouse',
                                                    'readonly': ~Eval('active'),
                                                },
                                                domain=[
                                                    ('type', 'in', ['storage', 'view']),
                                                    ('parent', 'child_of', [Eval('id')]),
                                                ],
                                                depends=['type', 'active', 'id'])

    production_output_location = fields.Many2One('stock.location',
                                                 'Production Output',
                                                 states={
                                                     'invisible': Eval('type') != 'warehouse',
                                                     'readonly': ~Eval('active'),
                                                 },
                                                 domain=[
                                                     ('type', '=', 'storage'),
                                                     ('parent', 'child_of', [Eval('id')]),
                                                 ],
                                                 depends=['type', 'active', 'id'])
